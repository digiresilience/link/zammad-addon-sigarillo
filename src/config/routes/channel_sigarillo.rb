Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match api_path + "/channels_sigarillo", to: "channels_sigarillo#index", via: :get
  match api_path + "/channels_sigarillo", to: "channels_sigarillo#add", via: :post
  match api_path + "/channels_sigarillo/:id", to: "channels_sigarillo#update", via: :put
  match api_path + "/channels_sigarillo_disable", to: "channels_sigarillo#disable", via: :post
  match api_path + "/channels_sigarillo_enable", to: "channels_sigarillo#enable", via: :post
  match api_path + "/channels_sigarillo", to: "channels_sigarillo#destroy", via: :delete
end
