class SigarilloChannel < ActiveRecord::Migration[5.2]
  def self.up
    p = Permission.find_by(name: "admin.channel_sigarillo")
    if p.nil?
      Permission.create_if_not_exists(
        name: "admin.channel_sigarillo",
        note: "Manage %s",
        preferences: {
          translations: ["Channel - Sigarillo"],
        },
      )
    end

    t = Ticket::Article::Type.find_by(name: "sigarillo personal-message")
    if t.nil?
      Ticket::Article::Type.create(
        name: "sigarillo personal-message",
        communication: true,
        updated_by_id: 1,
        created_by_id: 1,
      )
    end
  end

  def self.down
    a = Permission.find_by(name: "admin.channel_sigarillo")
    if !a.nil?
      a.destroy
    end

    a = Ticket::Article::Type.find_by(name: "sigarillo personal-message")
    if !a.nil?
      a.destroy
    end
  end
end
